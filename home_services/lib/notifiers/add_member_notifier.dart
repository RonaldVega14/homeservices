import 'dart:io';

import 'package:flutter/material.dart';

class AddMemberNotifier with ChangeNotifier {
  //Status Selector in Input form
  int _statusSelector = 0;

  int statusSelector() => _statusSelector;

  void changeStatus(int status) {
    _statusSelector = status;
    notifyListeners();
  }

  //Position Dropdown in Input form
  String _positionDropdown = 'Select a position';

  String positionDropdown() => _positionDropdown;

  void changePosition(String position) {
    _positionDropdown = position;
    notifyListeners();
  }

  //Image in input form
  File _image;

  File image() => _image;

  void changeImage(File image) {
    _image = image;
    notifyListeners();
  }

  //Date of Birth picker
  String _birthDate;

  String birthDate() => _birthDate;

  void changeBirthDate(DateTime date) {
    _birthDate = date.month.toString() +
        '/' +
        date.day.toString() +
        '/' +
        date.year.toString();
    notifyListeners();
  }
}
