import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

@immutable
class User {
  const User({@required this.uid});
  final String uid;
}

class FirebaseAuthService {
  bool _splashPage = true;

  bool get onSplashPage => _splashPage;

  final _firebaseAuth = FirebaseAuth.instance;

  User _userFromFirebase(FirebaseUser user) {
    return user == null ? null : User(uid: user.uid);
  }

  Stream<User> get onAuthStateChanged {
    return _firebaseAuth.onAuthStateChanged.map(_userFromFirebase);
  }

  Future<User> signIn(String email, String pass) async {
    final authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: pass);
    print('uid: ${authResult.user.uid}');
    return _userFromFirebase(authResult.user);
  }

  Future<void> signOut() async {
    return await _firebaseAuth.signOut();
  }

  void splashScreenOver() {
    _splashPage = false;
  }
}
