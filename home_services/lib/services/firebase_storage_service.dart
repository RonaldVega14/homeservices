import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';

class FirebaseStorageService {
  FirebaseStorageService({@required this.uid}) : assert(uid != null);
  final String uid;

  /// Upload an avatar from file
  Future<String> uploadAvatar(
          {@required String uid,
          @required File file,
          @required String collection}) async =>
      await upload(
        uid: uid,
        file: file,
        path: '$collection/$uid/downloadUrl.png',
        contentType: 'image/png',
      );

  /// Generic file upload for any [path] and [contentType]
  Future<String> upload({
    @required String uid,
    @required File file,
    @required String path,
    @required String contentType,
  }) async {
    //getting data
    print('uploading to: $path');
    final storageReference = FirebaseStorage.instance.ref().child(path);
    final uploadTask = storageReference.putFile(
        file, StorageMetadata(contentType: contentType));
    final snapshot = await uploadTask.onComplete;
    if (snapshot.error != null) {
      print('upload error code: ${snapshot.error}');
      throw snapshot.error;
    }
    // Url used to download file/image
    final downloadUrl = await snapshot.ref.getDownloadURL();
    print('downloadUrl: $downloadUrl');
    return downloadUrl;
  }

  Future<QuerySnapshot> getCollectionDocuments({@required collection}) async {
    return Firestore.instance.collection(collection).getDocuments();
  }

  Stream<QuerySnapshot> getCollectionStream({@required collection}) {
    return Firestore.instance.collection(collection).snapshots();
  }

  Stream<QuerySnapshot> getSubCollectionStream(
      {@required collection, @required subCollection, @required id}) {
    return Firestore.instance
        .collection(collection)
        .document(id)
        .collection(subCollection)
        .snapshots();
  }
}
