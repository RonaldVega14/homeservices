import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:home_services/models/avatar_reference.dart';

class FirestoreService {
  FirestoreService({@required this.uid}) : assert(uid != null);
  final String uid;
  // Sets the avatar download url
  Future<void> setAvatarReference({
    @required String imageUID,
    @required AvatarReference avatarReference,
    @required String collection,
  }) async {
    final path = collection + '/$imageUID';
    final reference = Firestore.instance.document(path);
    await reference.setData(avatarReference.toMap());
  }

  // Reads the current avatar download url
  Stream<AvatarReference> avatarReferenceStream({
    @required String imageUID,
    @required String collection,
  }) {
    final path = collection + '/$imageUID';
    final reference = Firestore.instance.document(path);
    final snapshots = reference.snapshots();
    return snapshots.map((snapshot) => AvatarReference.fromMap(snapshot.data));
  }
}
