import 'package:flutter/material.dart';
import 'package:home_services/pages/home_page.dart';
import 'package:home_services/pages/sign_in_page.dart';
import 'package:home_services/services/firebase_auth_service.dart';

class AuthWidget extends StatelessWidget {
  const AuthWidget({Key key, @required this.userSnapshot}) : super(key: key);
  final AsyncSnapshot<User> userSnapshot;
  @override
  Widget build(BuildContext context) {
    if (userSnapshot.connectionState == ConnectionState.active) {
      print('Connection State: $ConnectionState');
      return userSnapshot.hasData ? HomePage() : SignInPage();
    }
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
