import 'package:flutter/material.dart';
import 'package:home_services/controllers/auth_widget_builder.dart';
import 'package:home_services/services/firebase_auth_service.dart';
import 'package:home_services/services/image_picker_service.dart';
import 'package:provider/provider.dart';

import 'controllers/auth_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider<FirebaseAuthService>(
            create: (_) => FirebaseAuthService(),
          ),
          Provider<ImagePickerService>(
            create: (_) => ImagePickerService(),
          ),
        ],
        child: AuthWidgetBuilder(builder: (context, userSnapshot) {
          return MaterialApp(
            home: AuthWidget(userSnapshot: userSnapshot),
          );
        }));
  }
}
