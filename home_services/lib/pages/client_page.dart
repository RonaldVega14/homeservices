import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:home_services/models/avatar_reference.dart';
import 'package:home_services/services/firebase_auth_service.dart';
import 'package:home_services/services/firebase_storage_service.dart';
import 'package:home_services/services/firestore_service.dart';
import 'package:home_services/utils/my_flutter_app_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class ClientsPage extends StatefulWidget {
  ClientsPage({Key key}) : super(key: key);

  @override
  _ClientPageState createState() => _ClientPageState();
}

class _ClientPageState extends State<ClientsPage> {
  List<DocumentSnapshot> clientList;
  bool clientFlag = false;

  @override
  void initState() {
    super.initState();
    var database = Provider.of<FirebaseStorageService>(context, listen: false);
    database
        .getCollectionDocuments(collection: 'clients')
        .then((QuerySnapshot docs) {
      if (docs.documents.isNotEmpty) {
        clientList = docs.documents;
      }
      setState(() {
        clientFlag = true;
      });
    });
  }

  TextStyle style16 = TextStyle(
      fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.blue[900]);
  //Appbar
  Widget appbar(BuildContext context) {
    return AppBar(
      title: Center(
        child: Container(
          padding: EdgeInsets.only(left: 75.0),
          child: AutoSizeText(
            'House Name',
            maxLines: 1,
            maxFontSize: 16.0,
            minFontSize: 13.0,
            style: style16.copyWith(color: Colors.blue[900]),
          ),
        ),
      ),
      actions: <Widget>[
        Center(
          widthFactor: 1.5,
          child: Row(
            children: <Widget>[
              AutoSizeText(
                'Logout',
                maxLines: 1,
                maxFontSize: 15.0,
                minFontSize: 12.0,
                style: style16.copyWith(
                    fontWeight: FontWeight.bold, color: Colors.red),
              ),
              InkWell(
                onTap: () {
                  Provider.of<FirebaseAuthService>(context, listen: false)
                      .signOut();
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    MyFlutterApp.exit,
                    color: Colors.red,
                    size: 22.5,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
      leading: Icon(Icons.menu, color: Colors.blue[900]),
      elevation: 0.0,
      bottomOpacity: 0,
      backgroundColor: Colors.white,
    );
  }

//Construye el texto alineado al centro
  Widget _buildText(String title, Color color) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: AutoSizeText(title,
          maxFontSize: 12.0,
          minFontSize: 10.0,
          maxLines: 2,
          textAlign: TextAlign.center,
          style: style16.copyWith(
              color: color, fontWeight: FontWeight.bold, fontSize: 12.0)),
    );
  }

//Construye el texto del titulo centrado
  Widget buildTitleText() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
          ),
          Image.asset('assets/houseLogo.png', fit: BoxFit.contain),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );
  }

//Construye la columna de informacion
  Widget _buildTextColumn(var client) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
          child: _buildText('Name: ' + client['name'], Colors.white),
          flex: 2,
          fit: FlexFit.loose,
        ),
      ],
    );
  }

  Future<void> _chooseAvatar(BuildContext context, String clientID) async {
    try {
      //obtaining image from gallery
      final file = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (file != null) {
        //Getting storage service
        final storage =
            Provider.of<FirebaseStorageService>(context, listen: false);
        final downloadUrl = await storage.uploadAvatar(
            uid: clientID, file: file, collection: 'clients');
        //Saving to firestore
        final database = Provider.of<FirestoreService>(context, listen: false);
        await database.setAvatarReference(
            collection: 'client',
            imageUID: clientID,
            avatarReference: AvatarReference(downloadUrl));
        //Deleting local file
        await file.delete();
      }
    } catch (e) {
      print(e);
    }
  }

//Construye el avatar
  Widget _buildAvatar(String clientD, Color color, BuildContext context) {
    final database = Provider.of<FirestoreService>(context);
    //print('uid: ' + userID);
    return StreamBuilder<AvatarReference>(
        stream: database.avatarReferenceStream(
            imageUID: clientD, collection: 'clients'),
        builder: (context, snapshot) {
          //url obtenida desde un link
          if (snapshot.connectionState == ConnectionState.active) {
            final avatarReference = snapshot.data;
            return Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: color,
                  width: 2,
                ),
              ),
              child: InkWell(
                onTap: () => _chooseAvatar(context, clientD),
                //borderRadius: BorderRadius.circular(radius * 1.2),
                child: CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.black12,
                  backgroundImage: (avatarReference.downloadUrl != null &&
                          avatarReference.downloadUrl != '')
                      ? NetworkImage(avatarReference.downloadUrl)
                      : null,
                  child: (avatarReference.downloadUrl == null ||
                          avatarReference.downloadUrl == '')
                      ? Icon(
                          Icons.camera_alt,
                          size: 40,
                          color: color.withOpacity(0.7),
                        )
                      : null,
                ),
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

//Construye las columnas
  Widget _buildColumn(
      String clientID, Color color, BuildContext context, var client) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        //Avatar
        Flexible(
            child: _buildAvatar(clientID, color, context),
            flex: 6,
            fit: FlexFit.loose),
        //Text
        Flexible(child: _buildTextColumn(client), flex: 3, fit: FlexFit.tight),
      ],
    );
  }

//Card item
  Widget myItems(
      String uid, Color color, int event, BuildContext context, var client) {
    /*Se debe reemplazar el AuthenticationBloc por el 
  Bloc que se creara para los Eventos dentro del Home*/
    return Material(
      color: Colors.transparent,
      child: Container(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(24.0),
          highlightColor: Colors.blue[900],
          onTap: () {},
          child: Material(
            color: Colors.blue[900],
            elevation: 12.0,
            shadowColor: Colors.black,
            borderRadius: BorderRadius.circular(24.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: _buildColumn(uid, color, context, client),
                  flex: 1,
                  fit: FlexFit.tight,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

//Tablet items

//Construye el avatar
  Widget _buildTabletAvatar(String clientID, Color color) {
    final database = Provider.of<FirestoreService>(context);
    //print('uid: ' + userID);
    return StreamBuilder<AvatarReference>(
        stream: database.avatarReferenceStream(
            imageUID: clientID, collection: 'clients'),
        builder: (context, snapshot) {
          //url obtenida desde un link
          if (snapshot.connectionState == ConnectionState.active) {
            final avatarReference = snapshot.data;
            return Center(
              child: Container(
                height: 140,
                width: 140,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: color,
                    width: 2,
                  ),
                ),
                child: InkWell(
                  onTap: () => _chooseAvatar(context, clientID),
                  //borderRadius: BorderRadius.circular(radius * 1.2),
                  child: CircleAvatar(
                    radius: 50,
                    backgroundColor: Colors.black12,
                    backgroundImage: (avatarReference.downloadUrl != null &&
                            avatarReference.downloadUrl != '')
                        ? NetworkImage(avatarReference.downloadUrl)
                        : null,
                    child: (avatarReference.downloadUrl == null ||
                            avatarReference.downloadUrl == '')
                        ? Icon(
                            Icons.camera_alt,
                            size: 40,
                            color: color.withOpacity(0.7),
                          )
                        : null,
                  ),
                ),
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

//Construye el texto alineado a la izquierda
  Widget _buildTabletText(String title, Color color) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(title,
          textAlign: TextAlign.left,
          style: style16.copyWith(color: color, fontWeight: FontWeight.bold)),
    );
  }

  //Construye la columna de informacion
  Widget _buildTabletTextColumn(var client, Color color) {
    int progress =
        int.parse(client['progress'].toString().split('/').elementAt(0));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
            child: Container(color: Colors.transparent),
            flex: 1,
            fit: FlexFit.tight),
        Flexible(
          child: _buildTabletText('Name: ' + client['name'], color),
          flex: 5,
          fit: FlexFit.tight,
        ),
        Flexible(
          child: _buildTabletText('Phone number: ' + client['phone'], color),
          flex: 5,
          fit: FlexFit.tight,
        ),
        Flexible(
          child: _buildTabletText(
              'Progress: ' + client['progress'],
              progress < 5
                  ? Colors.red
                  : progress < 8 ? Colors.yellow[800] : Colors.green),
          flex: 5,
          fit: FlexFit.tight,
        ),
        Flexible(
          child: _buildTabletText('Status: ' + client['status'], color),
          flex: 5,
          fit: FlexFit.tight,
        ),
        Flexible(
            child: Container(color: Colors.transparent),
            flex: 1,
            fit: FlexFit.tight)
      ],
    );
  }

//Construye las columnas
  Widget _buildTabletRow(String clientID, Color color, var client) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        //Avatar
        Flexible(
            child: _buildTabletAvatar(
              clientID,
              color,
            ),
            flex: 2,
            fit: FlexFit.tight),
        Container(
          height: 150,
          width: 2,
          color: color,
        ),
        //Text
        Flexible(
            child: _buildTabletTextColumn(client, color),
            flex: 4,
            fit: FlexFit.loose),
      ],
    );
  }

//Card item
  Widget myTabletItems(String clientID, Color color, int event, var client) {
    /*Se debe reemplazar el AuthenticationBloc por el 
  Bloc que se creara para los Eventos dentro del Home*/
    return Material(
      color: Colors.transparent,
      child: Container(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(24.0),
          highlightColor: Colors.blue[900],
          onTap: () {},
          child: Material(
            color: Colors.white,
            elevation: 12.0,
            shadowColor: Colors.black,
            borderRadius: BorderRadius.circular(24.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: _buildTabletRow(clientID, color, client),
                  flex: 1,
                  fit: FlexFit.tight,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

//Junta y construye todo el contenido
  Widget buildContent(BuildContext context, bool useMobileLayout) {
    return Container(
      child: Center(
        child: clientFlag
            ? StaggeredGridView.countBuilder(
                crossAxisCount: 2,
                crossAxisSpacing: 20.0,
                mainAxisSpacing: 20.0,
                itemCount: clientList.length,
                padding:
                    const EdgeInsets.symmetric(vertical: 9.0, horizontal: 50.0),
                itemBuilder: (BuildContext context, int index) {
                  var client = clientList[index].data;
                  return useMobileLayout
                      ? myItems(clientList[index].documentID, Colors.blue[900],
                          index, context, client)
                      : myTabletItems(clientList[index].documentID,
                          Colors.blue[900], index, client);
                },
                staggeredTileBuilder: (int index) {
                  return StaggeredTile.extent(
                      1, MediaQuery.of(context).size.height * 0.22);
                },
              )
            : CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double shortestSide = MediaQuery.of(context).size.shortestSide;
    final bool useMobileLayout = shortestSide < 600;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appbar(context),
      body: SafeArea(
        child: Container(
          child: Stack(
            fit: StackFit.loose,
            children: <Widget>[
              Container(
                  height: MediaQuery.of(context).size.height * 0.14,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: buildTitleText(),
                  )),
              Container(
                margin: EdgeInsets.all(10.0),
                height: MediaQuery.of(context).size.height * 0.29,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'My Client',
                      style: style16.copyWith(
                          color: Colors.blue[900], fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 150),
                  child: buildContent(context, useMobileLayout)),
            ],
          ),
        ),
      ),
    );
  }
}
