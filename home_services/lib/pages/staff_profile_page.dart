import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:home_services/animations/fade_animation.dart';
import 'package:home_services/models/avatar_reference.dart';
import 'package:home_services/models/staff_model.dart';
import 'package:home_services/services/firebase_auth_service.dart';
import 'package:home_services/services/firebase_storage_service.dart';
import 'package:home_services/services/firestore_service.dart';
import 'package:home_services/utils/my_flutter_app_icons.dart';
import 'package:provider/provider.dart';

TextStyle style =
    TextStyle(fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.black);

class StaffProfile extends StatelessWidget {
  final Staff staff;
  const StaffProfile({Key key, @required this.staff}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final database =
        Provider.of<FirebaseStorageService>(context, listen: false);
    final double shortestSide = MediaQuery.of(context).size.shortestSide;
    final bool useMobileLayout = shortestSide < 600;

    Widget _appbar(BuildContext context) {
      return AppBar(
        title: Center(
          child: Container(
            padding: EdgeInsets.only(left: 75.0),
            child: AutoSizeText(
              'House Name',
              maxLines: 1,
              maxFontSize: 16.0,
              minFontSize: 13.0,
              style: style.copyWith(color: Colors.blue[900]),
            ),
          ),
        ),
        actions: <Widget>[
          Center(
            widthFactor: 1.5,
            child: InkWell(
              onTap: () {
                Provider.of<FirebaseAuthService>(context, listen: false)
                    .signOut();
              },
              child: Row(
                children: <Widget>[
                  AutoSizeText(
                    'Logout',
                    maxLines: 1,
                    maxFontSize: 15.0,
                    minFontSize: 12.0,
                    style: style.copyWith(
                        fontWeight: FontWeight.bold, color: Colors.red),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      MyFlutterApp.exit,
                      color: Colors.red,
                      size: 22.5,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
        leading: Icon(Icons.menu, color: Colors.blue[900]),
        elevation: 0.0,
        bottomOpacity: 0,
        backgroundColor: Colors.white,
      );
    }

    //Construye el avatar
    Widget _buildAvatar(String staffID, Color color, BuildContext context) {
      final database = Provider.of<FirestoreService>(context);
      //print('uid: ' + userID);
      return StreamBuilder<AvatarReference>(
          stream: database.avatarReferenceStream(
              imageUID: staffID, collection: 'staff'),
          builder: (context, snapshot) {
            //url obtenida desde un link
            if (snapshot.connectionState == ConnectionState.active) {
              final avatarReference = snapshot.data;
              return Center(
                  child: Container(
                height: 140,
                width: 140,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: color,
                    width: 2,
                  ),
                ),
                child: CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.black12,
                  backgroundImage: (avatarReference.downloadUrl != null &&
                          avatarReference.downloadUrl != '')
                      ? NetworkImage(avatarReference.downloadUrl)
                      : null,
                  child: (avatarReference.downloadUrl == null ||
                          avatarReference.downloadUrl == '')
                      ? Icon(
                          Icons.camera_alt,
                          size: 40,
                          color: color.withOpacity(0.7),
                        )
                      : null,
                ),
              ));
            } else {
              return CircularProgressIndicator();
            }
          });
    }

    Widget _informationText(String key, String value) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0, left: 24.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Flexible(
              flex: 2,
              fit: FlexFit.tight,
              child: Text(
                key,
                style: style,
              ),
            ),
            Flexible(flex: 1, fit: FlexFit.tight, child: SizedBox()),
            Flexible(
              flex: 4,
              fit: FlexFit.loose,
              child: Text(value, style: style),
            )
          ],
        ),
      );
    }

    Widget _informationTextTab(String key, String value) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0, left: 24.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Flexible(
              flex: 2,
              fit: FlexFit.tight,
              child: Text(
                key,
                style: style,
              ),
            ),
            Flexible(flex: 1, fit: FlexFit.tight, child: SizedBox()),
            Flexible(
              flex: 6,
              fit: FlexFit.loose,
              child: Text(value, style: style),
            )
          ],
        ),
      );
    }

    Widget _staffInformation(Staff staff) {
      return ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: _buildAvatar(staff.id, Colors.blue[900], context),
          ),
          SizedBox(
            height: 20.0,
          ),
          _informationText('Name:', staff.employeeName),
          SizedBox(
            height: 5.0,
          ),
          _informationText('Phone:', staff.phone),
          SizedBox(
            height: 10.0,
          ),
          Text(
            'Emergency Contact Information',
            style: style.copyWith(fontSize: 20.0, color: Colors.blue[900]),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 10.0,
          ),
          _informationText('Emergency Name:', staff.emergencyName),
          SizedBox(
            height: 5.0,
          ),
          _informationText('Emergency Phone:', staff.emergencyPhone),
        ],
      );
    }

    Widget _trainingItem(
        String training, String completionDate, bool status, String frecuency) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 4,
            fit: FlexFit.tight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(training, style: style),
            ),
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(completionDate, style: style),
            ),
          ),
          Flexible(
            flex: 3,
            fit: FlexFit.tight,
            child: Text('Every ' + frecuency + ' year(s)', style: style),
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: Text(status ? 'Completed' : 'Pending',
                style:
                    style.copyWith(color: status ? Colors.green : Colors.red)),
          ),
        ],
      );
    }

    Widget _formItem(String form, bool status) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 4,
            fit: FlexFit.tight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(form, style: style),
            ),
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: Text(status ? 'Completed' : 'Pending',
                style:
                    style.copyWith(color: status ? Colors.green : Colors.red)),
          ),
        ],
      );
    }

    Widget _trainingTitle() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 4,
            fit: FlexFit.tight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child:
                  Text('Name', style: style.copyWith(color: Colors.blue[900])),
            ),
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Completion \nDate',
                  style: style.copyWith(color: Colors.blue[900])),
            ),
          ),
          Flexible(
            flex: 3,
            fit: FlexFit.tight,
            child: Text('Frecuency',
                style: style.copyWith(color: Colors.blue[900])),
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child:
                Text('Status', style: style.copyWith(color: Colors.blue[900])),
          ),
        ],
      );
    }

    Widget _staffTrainingsStream(Staff staff) {
      return StreamBuilder<QuerySnapshot>(
        stream: database.getSubCollectionStream(
            collection: 'staff', subCollection: 'trainings', id: staff.id),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text('Loading...', style: style)
                ],
              ));
            default:
              return new Column(
                children:
                    snapshot.data.documents.map((DocumentSnapshot document) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: CustomFadeAnimation(
                        0.4,
                        _trainingItem(
                            document.data['name'],
                            document.data['completionDate'],
                            document.data['status'],
                            document.data['frecuency'].toString())),
                  );
                }).toList(),
              );
          }
        },
      );
    }

    Widget _staffFormsStream(Staff staff) {
      return StreamBuilder<QuerySnapshot>(
        stream: database.getSubCollectionStream(
            collection: 'staff', subCollection: 'forms', id: staff.id),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text('Loading...', style: style)
                ],
              ));
            default:
              return new Column(
                children:
                    snapshot.data.documents.map((DocumentSnapshot document) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: CustomFadeAnimation(
                        0.4,
                        _formItem(
                            document.data['name'], document.data['status'])),
                  );
                }).toList(),
              );
          }
        },
      );
    }

    Widget _subTitle(String title) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(title, style: style.copyWith(color: Colors.blue[900])),
        ),
      );
    }

    Widget _trainingsAndCertifications(Staff staff) {
      return ListView(
        children: <Widget>[
          CustomFadeAnimation(0.4, _subTitle('Trainings and Certifications')),
          SizedBox(
            height: 10.0,
          ),
          CustomFadeAnimation(0.4, _trainingTitle()),
          SizedBox(
            height: 15.0,
          ),
          _staffTrainingsStream(staff),
          SizedBox(
            height: 15.0,
          ),
          CustomFadeAnimation(0.4, _subTitle('Forms')),
          _staffFormsStream(staff),
          SizedBox(
            height: 10.0,
          )
        ],
      );
    }

    Widget _staffInformationTab(Staff staff) {
      print('Staff Education\n${staff.education}');
      return ListView(
        children: <Widget>[
          _subTitle('Information'),
          SizedBox(
            height: 15.0,
          ),
          _informationTextTab('Name: ', staff.employeeName),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Phone: ', staff.phone),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Address: ', staff.address),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Position: ', staff.position),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Date Of Hire: ', staff.dateOfHire),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Birth Date: ', staff.dateOfBirth),
          SizedBox(
            height: 5.0,
          ),
          _subTitle('Emergency Contact Information'),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Emergency Name: ', staff.emergencyName),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Emergency Phone: ', staff.emergencyPhone),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Emergency Address: ', staff.emergencyAddress),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Emergency Relation: ', staff.relationship),
          SizedBox(
            height: 5.0,
          ),
          _subTitle('High School Education'),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('High School: ', staff.education.highSchoolName),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Address: ', staff.education.highSchoolAddress),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('From: ', staff.education.highSchoolFrom),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('To: ', staff.education.highSchoolTo),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Graduated: ',
              staff.education.highSchoolGraduated ? 'Yes' : 'No'),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Diploma: ', staff.education.highSchoolDiploma),
          SizedBox(
            height: 5.0,
          ),
          _subTitle('College Education'),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('College: ', staff.education.collegeName),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Address: ', staff.education.collegeAddress),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('From: ', staff.education.collegeFrom),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('To: ', staff.education.collegeTo),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab(
              'Graduated: ', staff.education.collegeGraduated ? 'Yes' : 'No'),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Diploma: ', staff.education.collegeDiploma),
          SizedBox(
            height: 5.0,
          ),
          _subTitle('Other Education'),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Other Education: ', staff.education.otherName),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Address: ', staff.education.otherAddress),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('From: ', staff.education.otherFrom),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('To: ', staff.education.otherTo),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab(
              'Graduate: ', staff.education.otherGraduated ? 'Yes' : 'No'),
          SizedBox(
            height: 5.0,
          ),
          _informationTextTab('Diploma: ', staff.education.otherDiploma),
          SizedBox(
            height: 5.0,
          ),
        ],
      );
    }

    Widget _tab(String title) {
      return Tab(
          child: Container(
        child: Align(
          alignment: Alignment.center,
          child: Text(
            title,
            style: TextStyle(fontFamily: 'Montserrat', fontSize: 16.0),
          ),
        ),
      ));
    }

    Widget _tabSide(Staff staff) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 1,
            fit: FlexFit.loose,
            child: TabBar(
              unselectedLabelColor: Colors.blue[900],
              indicator: BoxDecoration(
                  color: Colors.blue[900],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(14.0),
                      topRight: Radius.circular(14.0))),
              tabs: <Widget>[
                _tab('Information'),
                _tab('Trainings and Certifications'),
              ],
            ),
          ),
          Flexible(
            flex: 8,
            fit: FlexFit.tight,
            child: TabBarView(
              children: <Widget>[
                _staffInformationTab(staff),
                _trainingsAndCertifications(staff),
              ],
            ),
          )
        ],
      );
    }

//-----------------------------------------------------------------------BUILD WIDGET TO THE SCREEN-----------------------------------------------------------------------
    return Scaffold(
      appBar: _appbar(context),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
              flex: 3,
              fit: FlexFit.tight,
              child: _staffInformation(staff),
            ),
            Center(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.8,
                width: 1.0,
                color: Colors.blue[900],
              ),
            ),
            Flexible(
              flex: 7,
              fit: FlexFit.tight,
              child: DefaultTabController(length: 2, child: _tabSide(staff)),
            )
          ],
        ),
      ),
    );
  }
}
