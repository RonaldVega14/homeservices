import 'package:flutter/material.dart';
import 'package:home_services/animations/fade_animation.dart';
import 'package:home_services/services/firebase_auth_service.dart';
import 'package:provider/provider.dart';

TextStyle style =
    TextStyle(fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.black);
String _email;
String _password;

class SignInPage extends StatelessWidget {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();

//Buttons
  Widget twoButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: loginButton(context),
          ),
        ),
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: singUpButton,
          ),
        ),
      ],
    );
  }

//SignUp
  final singUpButton = Material(
    elevation: 5.0,
    borderRadius: BorderRadius.circular(12.0),
    color: Colors.blue[900],
    child: MaterialButton(
      padding: EdgeInsets.fromLTRB(20.0, 3.0, 20.0, 3.0),
      onPressed: () {},
      child: Text("Sing Up",
          textAlign: TextAlign.center,
          style:
              style.copyWith(color: Colors.white, fontWeight: FontWeight.bold)),
    ),
  );
//Login
  Widget loginButton(BuildContext context) => Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(12.0),
        color: Colors.blue[700],
        child: MaterialButton(
          padding: EdgeInsets.fromLTRB(20.0, 3.0, 20.0, 3.0),
          onPressed: () => _signIn(context),
          child: Text("Login",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      );

//Metodo para ingresar a la pagina.
  void _signIn(BuildContext context) async {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();

      try {
        final auth = Provider.of<FirebaseAuthService>(context, listen: false);
        final user = await auth.signIn(_email, _password);
        print('uid: ${user.uid}');
      } catch (e) {
        print(e);
      }
    }
  }

// Input Fields
//Password
  final passwordField = TextFormField(
    validator: (val) => !(val.length > 6) ? 'Invalid Password' : null,
    onSaved: (val) => _password = val,
    obscureText: true,
    style: TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Colors.black),
    decoration: InputDecoration(
        labelText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12.0))),
  );

//Email
  final emailField = TextFormField(
    validator: (val) => !val.contains('@') ? 'Invalid Email' : null,
    onSaved: (val) => _email = val,
    style: TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Colors.black),
    decoration: InputDecoration(
        labelText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12.0))),
  );
  @override
  Widget build(BuildContext context) {
    final double shortestSide = MediaQuery.of(context).size.shortestSide;
    final bool useMobileLayout = shortestSide < 600;
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        key: scaffoldKey,
        body: Center(
          child: new Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                child: Padding(
                    padding: const EdgeInsets.all(36.0),
                    child: SingleChildScrollView(
                      padding: const EdgeInsets.all(5.0),
                      reverse: true,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
//Este SizedBox sirve para crear un espacio definido donde poner la imagen o Logo.
                          CustomFadeAnimation(
                            1,
                            SizedBox(
                              height: 150.0,
                              child: Image.asset(
                                "assets/houseLogo.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
//Estos SizedBox sirven para crear espacios en blanco entre un campo y otro.
                          SizedBox(height: 45.0),
//EmailField esta creado arriba al igual que los demas campos.(passwordField y loginButton)
                          useMobileLayout
                              ? CustomFadeAnimation(1.3, emailField)
                              : Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.4,
                                  child: CustomFadeAnimation(1.3, emailField)),
                          SizedBox(height: 25.0),
                          useMobileLayout
                              ? CustomFadeAnimation(1.3, passwordField)
                              : Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.4,
                                  child:
                                      CustomFadeAnimation(1.3, passwordField)),
                          SizedBox(height: 35.0),
                          useMobileLayout
                              ? CustomFadeAnimation(1.6, twoButtons(context))
                              : Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.4,
                                  child: CustomFadeAnimation(
                                      1.6, twoButtons(context))),
                          SizedBox(height: 15.0),
                        ],
                      ),
                    )),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
