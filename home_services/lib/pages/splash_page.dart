import 'package:flutter/material.dart';
import 'package:home_services/animations/fade_animation.dart';
import 'package:home_services/pages/sign_in_page.dart';
import 'package:home_services/services/firebase_auth_service.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  TextStyle style =
      TextStyle(fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.black);

  AnimationController _scaleController;
  AnimationController _scale2Controller;
  AnimationController _widthController;
  AnimationController _positionController;

  Animation<double> _scaleAnimation;
  Animation<double> _scale2Animation;
  Animation<double> _widthAnimation;
  Animation<double> _positionAnimation;

  bool hideIcon = false;

  @override
  void initState() {
    super.initState();

    //Creando animaciones para la pantalla de splash
    _scaleController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    _scaleAnimation =
        Tween<double>(begin: 1.0, end: 0.8).animate(_scaleController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              _widthController.forward();
            }
          });

    _widthController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 600));

    _widthAnimation =
        Tween<double>(begin: 80.0, end: 350.0).animate(_widthController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              _positionController.forward();
            }
          });

    _positionController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000));

    _positionAnimation =
        Tween<double>(begin: 0.0, end: 265.0).animate(_positionController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              setState(() {
                hideIcon = true;
              });
              _scale2Controller.forward();
            }
          });

    _scale2Controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    _scale2Animation =
        Tween<double>(begin: 1.0, end: 32.0).animate(_scale2Controller)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              Provider.of<FirebaseAuthService>(context).splashScreenOver();
              Navigator.of(context).push(
                MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (_) => SignInPage(),
                ),
              );
            }
          });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
      body: Container(
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomFadeAnimation(
                    1,
                    Text(
                      'Welcome to <AppName>',
                      style: style.copyWith(color: Colors.white, fontSize: 40),
                    ),
                  ),
                  SizedBox(height: 75.0),
                  CustomFadeAnimation(
                    1.3,
                    Text(
                      '“It’s up to you to make your life. Take what you have and stack it up like a tower of teetering blocks. Build your dream around that.” – Cheryl Strayed',
                      style: style.copyWith(color: Colors.white),
                    ),
                  ),
                  SizedBox(height: 120.0),
                  CustomFadeAnimation(
                    1.6,
                    AnimatedBuilder(
                      animation: _scaleController,
                      builder: (context, child) => Transform.scale(
                        scale: _scaleAnimation.value,
                        child: Center(
                            child: AnimatedBuilder(
                          animation: _widthController,
                          builder: (context, child) => Container(
                            padding: const EdgeInsets.all(12),
                            width: _widthAnimation.value,
                            height: 80,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.blue.withOpacity(0.4)),
                            child: InkWell(
                              onTap: () {
                                _scaleController.forward();
                              },
                              child: Stack(children: <Widget>[
                                AnimatedBuilder(
                                  animation: _positionController,
                                  builder: (context, child) => Positioned(
                                    left: _positionAnimation.value,
                                    child: AnimatedBuilder(
                                        animation: _scale2Controller,
                                        builder: (context, child) =>
                                            Transform.scale(
                                              scale: _scale2Animation.value,
                                              child: Container(
                                                width: 55,
                                                height: 55,
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: Colors.blue,
                                                ),
                                                child: hideIcon == false
                                                    ? Icon(
                                                        Icons.arrow_forward,
                                                        color: Colors.white,
                                                      )
                                                    : Container(),
                                              ),
                                            )),
                                  ),
                                ),
                              ]),
                            ),
                          ),
                        )),
                      ),
                    ),
                  ),
                  SizedBox(height: 60.0)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
