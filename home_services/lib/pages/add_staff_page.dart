import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:home_services/animations/fade_animation.dart';
import 'package:home_services/models/training_model.dart';
import 'package:home_services/services/firebase_auth_service.dart';
import 'package:home_services/services/firebase_storage_service.dart';
import 'package:home_services/utils/my_flutter_app_icons.dart';
import 'package:home_services/notifiers/add_member_notifier.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

TextStyle style16 =
    TextStyle(fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.black);

class AddMemberPage extends StatelessWidget {
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  List<String> _openPositions = [
    'Select a position',
    'Position 1',
    'Position 2',
    'Position 3',
  ];

  String _address,
      _phone,
      _name,
      _birthDate,
      _hireDate,
      _emergencyAddress,
      _emergencyName,
      _emergencyPhone,
      _emergencyRelation,
      _cprTraining;
  File _image;

  @override
  Widget build(BuildContext context) {
    final database =
        Provider.of<FirebaseStorageService>(context, listen: false);
    var addMemberNotifier =
        Provider.of<AddMemberNotifier>(context, listen: false);
    final double shortestSide = MediaQuery.of(context).size.shortestSide;
    final bool useMobileLayout = shortestSide < 600;
    print('Add member page build');
    Widget appbar(BuildContext context) {
      return AppBar(
        title: Center(
          child: Container(
            padding: EdgeInsets.only(left: 75.0),
            child: AutoSizeText(
              'House Name',
              maxLines: 1,
              maxFontSize: 16.0,
              minFontSize: 13.0,
              style: style16.copyWith(color: Colors.blue[900]),
            ),
          ),
        ),
        actions: <Widget>[
          Center(
            widthFactor: 1.5,
            child: Row(
              children: <Widget>[
                AutoSizeText(
                  'Logout',
                  maxLines: 1,
                  maxFontSize: 15.0,
                  minFontSize: 12.0,
                  style: style16.copyWith(
                      fontWeight: FontWeight.bold, color: Colors.red),
                ),
                InkWell(
                  onTap: () {
                    Provider.of<FirebaseAuthService>(context, listen: false)
                        .signOut();
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      MyFlutterApp.exit,
                      color: Colors.red,
                      size: 22.5,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
        leading: Icon(Icons.menu, color: Colors.blue[900]),
        elevation: 0.0,
        bottomOpacity: 0,
        backgroundColor: Colors.white,
      );
    }

    Widget _inputField(
        String validatorString, String onSavedVal, String label) {
      return TextFormField(
        validator: (val) => !val.contains('@') ? validatorString : null,
        onSaved: (val) => onSavedVal = val,
        style: style16.copyWith(fontSize: 14.0),
        decoration: InputDecoration(
            labelText: label,
            contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.0))),
      );
    }

    Future<void> _chooseAvatar() async {
      try {
        //obtaining _image from gallery
        _image = await ImagePicker.pickImage(source: ImageSource.camera);
        if (_image != null) {
          addMemberNotifier.changeImage(_image);
        }
      } catch (e) {
        print(e);
      }
      return _image;
    }

    //Construye el avatar
    Widget _buildAvatar() {
      return Center(
        child: Container(
          height: 120,
          width: 120,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              color: Colors.blue[900],
              width: 2,
            ),
          ),
          child: InkWell(
            onTap: () => _chooseAvatar(),
            child: Consumer<AddMemberNotifier>(
              builder: (_, addMemberNotifier, __) {
                return CircleAvatar(
                    backgroundImage: _image == null ? null : FileImage(_image),
                    radius: 50,
                    backgroundColor: Colors.black12,
                    child: addMemberNotifier.image() == null
                        ? Icon(
                            Icons.camera_alt,
                            size: 40,
                            color: Colors.blue[900].withOpacity(0.7),
                          )
                        : null);
              },
            ),
          ),
        ),
      );
    }

    Widget positionSelector() {
      return Padding(
        padding: const EdgeInsets.only(left: 14.0),
        child: Consumer<AddMemberNotifier>(builder: (_, addMemberNotifier, __) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(
                'Position: ',
                style: style16,
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(12.0),
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.7),
                    width: 2,
                  ),
                ),
                child: DropdownButton<String>(
                  underline: SizedBox(),
                  elevation: 4,
                  items: _openPositions.map((String position) {
                    return DropdownMenuItem<String>(
                      value: position,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: AutoSizeText(
                          position,
                          maxFontSize: 14,
                          minFontSize: 12,
                          maxLines: 1,
                          style: style16.copyWith(fontSize: 14.0),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                  }).toList(),
                  onChanged: (String newPosition) {
                    addMemberNotifier.changePosition(newPosition);
                  },
                  value: addMemberNotifier.positionDropdown(),
                ),
              ),
            ],
          );
        }),
      );
    }

    Widget _phoneAndPositionFields() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Flexible(
            flex: 4,
            fit: FlexFit.tight,
            child: _inputField('Invalid Phone Number', _phone, 'Phone Number'),
          ),
          Flexible(
            flex: 5,
            fit: FlexFit.tight,
            child: positionSelector(),
          ),
        ],
      );
    }

    Widget _inputDates() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 6,
            fit: FlexFit.loose,
            child: _inputField('Invalid Date', _birthDate, 'Date Of Birth'),
          ),
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: SizedBox(),
          ),
          Flexible(
            flex: 6,
            fit: FlexFit.loose,
            child: _inputField('Invalid Date', _hireDate, 'Date Of Hire'),
          )
        ],
      );
    }

    Widget _emergencyShortFields() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 6,
            fit: FlexFit.loose,
            child:
                _inputField('Invalid Phone', _emergencyPhone, 'Phone Number'),
          ),
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: SizedBox(),
          ),
          Flexible(
            flex: 6,
            fit: FlexFit.loose,
            child:
                _inputField('Invalid Relation', _emergencyRelation, 'Relation'),
          )
        ],
      );
    }

//------------------------------------------------------------------------------------------------
    void addMember() {}

    Widget addMemberButton() => Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(12.0),
          color: Colors.blue[900],
          child: MaterialButton(
            padding: EdgeInsets.fromLTRB(20.0, 3.0, 20.0, 3.0),
            onPressed: () => addMember(),
            child: Text("Add member",
                textAlign: TextAlign.center,
                style: style16.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
          ),
        );

    Widget button() {
      return Padding(
        padding: const EdgeInsets.only(bottom: 14.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(flex: 1, fit: FlexFit.tight, child: SizedBox()),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: addMemberButton(),
              ),
            ),
            Flexible(flex: 1, fit: FlexFit.tight, child: SizedBox()),
          ],
        ),
      );
    }

    Widget _subTitle(String title) {
      return Center(
        child: Text(title,
            style: style16.copyWith(color: Colors.blue[900], fontSize: 18)),
      );
    }

    Widget _inputInformation(bool useMobileLayout, var addMemberNotifier) {
      return Center(
        child: SingleChildScrollView(
          child: Container(
            width: useMobileLayout
                ? MediaQuery.of(context).size.width
                : MediaQuery.of(context).size.width * 0.45,
            color: Colors.white,
            child: new Form(
              key: formKey,
              child: Padding(
                  padding:
                      const EdgeInsets.only(left: 36.0, right: 36.0, top: 5.0),
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.all(5.0),
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        CustomFadeAnimation(1, _buildAvatar()),
                        SizedBox(height: 25.0),
                        CustomFadeAnimation(
                            1.3,
                            _inputField(
                                'Invalid Name', _name, 'Employee Name')),
                        SizedBox(height: 25.0),
                        CustomFadeAnimation(
                            1.3,
                            _inputField(
                                'Invalid Address', _address, 'Address')),
                        SizedBox(height: 25.0),
                        CustomFadeAnimation(1.3, _phoneAndPositionFields()),
                        SizedBox(height: 25.0),
                        CustomFadeAnimation(1.3, _inputDates()),
                        SizedBox(height: 25.0),
                        CustomFadeAnimation(
                            1.3, _subTitle('Emergency Information')),
                        SizedBox(height: 25.0),
                        CustomFadeAnimation(
                            1.3,
                            _inputField('Invalid Name', _emergencyName,
                                'Emergency Name')),
                        SizedBox(height: 25.0),
                        CustomFadeAnimation(
                            1.3,
                            _inputField('Invalid Address', _emergencyAddress,
                                'Address')),
                        SizedBox(height: 25.0),
                        CustomFadeAnimation(1.3, _emergencyShortFields()),
                      ],
                    ),
                  )),
            ),
          ),
        ),
      );
    }

//Right Side form

    Widget _attachButton() {
      return InkWell(
        onTap: () {},
        child: Container(
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            color: Colors.blue[900],
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(12.0),
            border: Border.all(
              color: Colors.blue[900],
              width: 2,
            ),
          ),
          child:
              Text('Attach file', style: style16.copyWith(color: Colors.white)),
        ),
      );
    }

    Widget _formItem(String formName, bool formValue) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 3,
            fit: FlexFit.loose,
            child: _attachButton(),
          ),
          Flexible(
            flex: 7,
            fit: FlexFit.tight,
            child: CheckboxListTile(
              checkColor: Colors.white,
              activeColor: Colors.blue[900],
              onChanged: (bool value) {},
              value: formValue,
              title: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(formName, style: style16),
              ),
            ),
          )
        ],
      );
    }

    Widget completionDateField() {
      return TextField(
        onSubmitted: (val) => _cprTraining = val,
        style: style16.copyWith(fontSize: 14.0),
        decoration: InputDecoration(
            labelText: 'mm/dd/yyyy',
            contentPadding: EdgeInsets.fromLTRB(15.0, 0, 5.0, 0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.0))),
      );
    }

    Widget _trainingItem(String training) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 3,
            fit: FlexFit.tight,
            child: Text(training, style: style16),
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.loose,
            child: completionDateField(),
          ),
          Flexible(
              flex: 3,
              fit: FlexFit.loose,
              child: Padding(
                padding: const EdgeInsets.only(left: 14.0),
                child: _attachButton(),
              )),
        ],
      );
    }

    Widget _trainingTitle() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 3,
            fit: FlexFit.tight,
            child: Text('Name',
                style: style16.copyWith(color: Colors.blue[900]),
                textAlign: TextAlign.center),
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.loose,
            child: Text('Completion Date',
                style: style16.copyWith(color: Colors.blue[900]),
                textAlign: TextAlign.center),
          ),
          Flexible(flex: 3, fit: FlexFit.loose, child: SizedBox()),
        ],
      );
    }

    Widget _trainingStream() {
      return StreamBuilder<QuerySnapshot>(
        stream: database.getCollectionStream(collection: 'trainings'),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text('Loading...', style: style16)
                ],
              ));
            default:
              return new Column(
                children:
                    snapshot.data.documents.map((DocumentSnapshot document) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: CustomFadeAnimation(
                        1.3, _trainingItem(document.data['name'])),
                  );
                }).toList(),
              );
          }
        },
      );
    }

    Widget _formsStream() {
      return StreamBuilder<QuerySnapshot>(
        stream: database.getCollectionStream(collection: 'forms'),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 25.0,
                  ),
                  Text('Loading...', style: style16)
                ],
              ));
            default:
              return new Column(
                children:
                    snapshot.data.documents.map((DocumentSnapshot document) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: CustomFadeAnimation(
                        1.3,
                        _formItem(
                            document.data['name'], document.data['status'])),
                  );
                }).toList(),
              );
          }
        },
      );
    }

    Widget _certificationsForm(bool useMobileLayout, var addMemberNotifier) {
      return Center(
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: useMobileLayout
                ? MediaQuery.of(context).size.width
                : MediaQuery.of(context).size.width * 0.45,
            color: Colors.white,
            child: Padding(
                padding:
                    const EdgeInsets.only(left: 36.0, right: 36.0, top: 5.0),
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(5.0),
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      CustomFadeAnimation(
                          1.3, _subTitle('Training and certifications')),
                      SizedBox(height: 20.0),
                      CustomFadeAnimation(1.3, _trainingTitle()),
                      SizedBox(height: 10.0),
                      _trainingStream(),
                      SizedBox(height: 25.0),
                      CustomFadeAnimation(1.3, _subTitle('Forms')),
                      SizedBox(height: 20.0),
                      _formsStream(),
                    ],
                  ),
                )),
          ),
        ),
      );
    }

    Widget _memberForm(bool useMobileLayout, var addMemberNotifier) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: _inputInformation(useMobileLayout, addMemberNotifier),
          ),
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: _certificationsForm(useMobileLayout, addMemberNotifier),
          )
        ],
      );
    }

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
          appBar: appbar(context),
          backgroundColor: Colors.white,
          key: scaffoldKey,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    child: CustomFadeAnimation(
                        1,
                        SizedBox(
                            height: 50.0,
                            child: Center(
                              child: Text('Add a Member',
                                  style: style16.copyWith(
                                      fontSize: 40, color: Colors.blue[900])),
                            )))),
              ),
              Flexible(
                flex: 9,
                fit: FlexFit.tight,
                child: _memberForm(useMobileLayout, addMemberNotifier),
              ),
              Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: CustomFadeAnimation(1.6, button()),
              )
            ],
          )),
    );
  }
}
