import 'dart:developer';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:home_services/models/avatar_reference.dart';
import 'package:home_services/models/staff_model.dart';
import 'package:home_services/pages/staff_profile_page.dart';
import 'package:home_services/services/firebase_auth_service.dart';
import 'package:home_services/services/firebase_storage_service.dart';
import 'package:home_services/services/firestore_service.dart';
import 'package:home_services/utils/my_flutter_app_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class StaffPage extends StatefulWidget {
  StaffPage({Key key}) : super(key: key);

  @override
  _StaffPageState createState() => _StaffPageState();
}

class _StaffPageState extends State<StaffPage> {
  List<DocumentSnapshot> tempStaffList;
  bool staffFlag = false;
  List<Staff> staffList = [];
  Staff tempStaff;
  Education tempEducation;

  @override
  void initState() {
    super.initState();
    var database = Provider.of<FirebaseStorageService>(context, listen: false);
    database
        .getCollectionDocuments(collection: 'staff')
        .then((QuerySnapshot docs) {
      if (docs.documents.isNotEmpty) {
        tempStaffList = docs.documents;
        for (var staff in tempStaffList) {
          Map<dynamic, dynamic> education = staff.data['education'];
          tempEducation = new Education(
              highSchoolName: education['highSchoolName'] ?? 'none',
              highSchoolAddress: education['highSchoolAddress'] ?? 'none',
              highSchoolDiploma: education['highSchoolDiploma'] ?? 'none',
              highSchoolFrom: education['highSchoolFrom'] == 0
                  ? 'none'
                  : education['highSchoolFrom'].toString(),
              highSchoolGraduated: education['highSchoolGraduated'],
              highSchoolTo: education['highSchoolTo'] == 0
                  ? 'none'
                  : education['highSchoolTo'].toString(),
              collegeAddress: education['collegeAddress'] ?? 'none',
              collegeDiploma: education['collegeDiploma'] ?? 'none',
              collegeFrom: education['collegeFrom'] == 0
                  ? 'none'
                  : education['collegeFrom'].toString(),
              collegeGraduated: education['collegeGraduated'],
              collegeName: education['collegeName'] ?? 'none',
              collegeTo: education['collegeTo'] == 0
                  ? 'none'
                  : education['collegeTo'].toString(),
              otherAddress: education['otherAddress'] ?? 'none',
              otherDiploma: education['otherDiploma'] ?? 'none',
              otherFrom: education['otherFrom'] == 0
                  ? 'none'
                  : education['otherFrom'].toString(),
              otherGraduated: education['otherGraduated'],
              otherName: education['otherName'] ?? 'none',
              otherTo: education['otherTo'] == 0
                  ? 'none'
                  : education['otherTo'].toString());
          tempStaff = new Staff(
              id: staff.documentID,
              employeeName: staff.data['employeeName'].toString(),
              address: staff.data['address'].toString(),
              dateOfBirth: staff.data['dateOfBirth'].toString(),
              dateOfHire: staff.data['dateOfHire'].toString(),
              downloadUrl: staff.data['downloadUrl'].toString(),
              emergencyAddress: staff.data['emergencyAddress'].toString(),
              emergencyName: staff.data['emergencyName'].toString(),
              emergencyPhone: staff.data['emergencyPhone'].toString(),
              phone: staff.data['phone'].toString(),
              position: staff.data['position'].toString(),
              relationship: staff.data['relationship'].toString(),
              education: tempEducation);
          print('\nEducation Info\n ${education['highSchoolGraduated']}');
          staffList.add(tempStaff);
          tempStaff = null;
        }
      }

      setState(() {
        staffFlag = true;
      });
    });
  }

  TextStyle style16 = TextStyle(
      fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.blue[900]);
  //Appbar
  Widget appbar(BuildContext context) {
    return AppBar(
      title: Center(
        child: Container(
          padding: EdgeInsets.only(left: 75.0),
          child: AutoSizeText(
            'House Name',
            maxLines: 1,
            maxFontSize: 16.0,
            minFontSize: 13.0,
            style: style16.copyWith(color: Colors.blue[900]),
          ),
        ),
      ),
      actions: <Widget>[
        Center(
          widthFactor: 1.5,
          child: Row(
            children: <Widget>[
              AutoSizeText(
                'Logout',
                maxLines: 1,
                maxFontSize: 15.0,
                minFontSize: 12.0,
                style: style16.copyWith(
                    fontWeight: FontWeight.bold, color: Colors.red),
              ),
              InkWell(
                onTap: () {
                  Provider.of<FirebaseAuthService>(context, listen: false)
                      .signOut();
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    MyFlutterApp.exit,
                    color: Colors.red,
                    size: 22.5,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
      leading: Icon(Icons.menu, color: Colors.blue[900]),
      elevation: 0.0,
      bottomOpacity: 0,
      backgroundColor: Colors.white,
    );
  }

//Construye el texto alineado al centro
  Widget _buildText(String title, Color color) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: AutoSizeText(title,
          maxFontSize: 12.0,
          minFontSize: 10.0,
          maxLines: 2,
          textAlign: TextAlign.center,
          style: style16.copyWith(
              color: color, fontWeight: FontWeight.bold, fontSize: 12.0)),
    );
  }

//Construye el texto del titulo centrado
  Widget buildTitleText() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
          ),
          Image.asset('assets/houseLogo.png', fit: BoxFit.contain),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );
  }

//Construye la columna de informacion
  Widget _buildTextColumn(Staff staff) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
          child: _buildText('Name: ' + staff.employeeName, Colors.white),
          flex: 2,
          fit: FlexFit.loose,
        ),
      ],
    );
  }

  Future<void> _chooseAvatar(BuildContext context, String staffID) async {
    try {
      //obtaining image from gallery
      final file = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (file != null) {
        //Getting storage service
        final storage =
            Provider.of<FirebaseStorageService>(context, listen: false);
        final downloadUrl = await storage.uploadAvatar(
            uid: staffID, file: file, collection: 'staff');
        //Saving to firestore
        final database = Provider.of<FirestoreService>(context, listen: false);
        await database.setAvatarReference(
            collection: 'staff',
            imageUID: staffID,
            avatarReference: AvatarReference(downloadUrl));
        //Deleting local file
        await file.delete();
      }
    } catch (e) {
      print(e);
    }
  }

//Construye el avatar
  Widget _buildAvatar(String staffID, Color color, BuildContext context) {
    final database = Provider.of<FirestoreService>(context);
    //print('uid: ' + userID);
    return StreamBuilder<AvatarReference>(
        stream: database.avatarReferenceStream(
            imageUID: staffID, collection: 'staff'),
        builder: (context, snapshot) {
          //url obtenida desde un link
          if (snapshot.connectionState == ConnectionState.active) {
            final avatarReference = snapshot.data;
            return Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: color,
                  width: 2,
                ),
              ),
              child: InkWell(
                onTap: () => _chooseAvatar(context, staffID),
                //borderRadius: BorderRadius.circular(radius * 1.2),
                child: CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.black12,
                  backgroundImage: (avatarReference.downloadUrl != null &&
                          avatarReference.downloadUrl != '')
                      ? NetworkImage(avatarReference.downloadUrl)
                      : null,
                  child: (avatarReference.downloadUrl == null ||
                          avatarReference.downloadUrl == '')
                      ? Icon(
                          Icons.camera_alt,
                          size: 40,
                          color: color.withOpacity(0.7),
                        )
                      : null,
                ),
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

//Construye las columnas
  Widget _buildColumn(
      String staffID, Color color, BuildContext context, Staff staff) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        //Avatar
        Flexible(
            child: _buildAvatar(staffID, color, context),
            flex: 6,
            fit: FlexFit.loose),
        //Text
        Flexible(child: _buildTextColumn(staff), flex: 3, fit: FlexFit.tight),
      ],
    );
  }

//Card item
  Widget myItems(
      String uid, Color color, int event, BuildContext context, Staff staff) {
    /*Se debe reemplazar el AuthenticationBloc por el 
  Bloc que se creara para los Eventos dentro del Home*/
    return Material(
      color: Colors.transparent,
      child: Container(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(24.0),
          highlightColor: Colors.blue[900],
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                fullscreenDialog: true,
                builder: (_) => StaffProfile(staff: staff),
              ),
            );
          },
          child: Material(
            color: Colors.blue[900],
            elevation: 12.0,
            shadowColor: Colors.black,
            borderRadius: BorderRadius.circular(24.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: _buildColumn(uid, color, context, staff),
                  flex: 1,
                  fit: FlexFit.tight,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

//Tablet items

//Construye el avatar
  Widget _buildTabletAvatar(String staffID, Color color) {
    final database = Provider.of<FirestoreService>(context);
    //print('uid: ' + userID);
    return StreamBuilder<AvatarReference>(
        stream: database.avatarReferenceStream(
            imageUID: staffID, collection: 'staff'),
        builder: (context, snapshot) {
          //url obtenida desde un link
          if (snapshot.connectionState == ConnectionState.active) {
            final avatarReference = snapshot.data;
            return Center(
              child: Container(
                height: 140,
                width: 140,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: color,
                    width: 2,
                  ),
                ),
                child: InkWell(
                  onTap: () => _chooseAvatar(context, staffID),
                  //borderRadius: BorderRadius.circular(radius * 1.2),
                  child: CircleAvatar(
                    radius: 50,
                    backgroundColor: Colors.black12,
                    backgroundImage: (avatarReference.downloadUrl != null &&
                            avatarReference.downloadUrl != '')
                        ? NetworkImage(avatarReference.downloadUrl)
                        : null,
                    child: (avatarReference.downloadUrl == null ||
                            avatarReference.downloadUrl == '')
                        ? Icon(
                            Icons.camera_alt,
                            size: 40,
                            color: color.withOpacity(0.7),
                          )
                        : null,
                  ),
                ),
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

//Construye el texto alineado a la izquierda
  Widget _buildTabletText(String title, Color color) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(title,
          textAlign: TextAlign.left,
          style: style16.copyWith(color: color, fontWeight: FontWeight.bold)),
    );
  }

  //Construye la columna de informacion
  Widget _buildTabletTextColumn(Staff staff) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
            child: Container(color: Colors.transparent),
            flex: 1,
            fit: FlexFit.tight),
        Flexible(
          child: _buildTabletText('Name: ' + staff.employeeName, Colors.white),
          flex: 5,
          fit: FlexFit.tight,
        ),
        Flexible(
          child: _buildTabletText('Position: ' + staff.position, Colors.white),
          flex: 5,
          fit: FlexFit.tight,
        ),
        Flexible(
          child: _buildTabletText('Phone number: ' + staff.phone, Colors.white),
          flex: 5,
          fit: FlexFit.tight,
        ),
        Flexible(
          child: _buildTabletText(
              'Emergency phone: ' + staff.emergencyPhone, Colors.white),
          flex: 5,
          fit: FlexFit.tight,
        ),
        Flexible(
            child: Container(color: Colors.transparent),
            flex: 1,
            fit: FlexFit.tight)
      ],
    );
  }

//Construye las columnas
  Widget _buildTabletRow(String staffID, Color color, Staff staff) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        //Avatar
        Flexible(
            child: _buildTabletAvatar(
              staffID,
              color,
            ),
            flex: 2,
            fit: FlexFit.tight),
        Container(
          height: 150,
          width: 2,
          color: Colors.white,
        ),
        //Text
        Flexible(
            child: _buildTabletTextColumn(staff), flex: 4, fit: FlexFit.loose),
      ],
    );
  }

//Card item
  Widget myTabletItems(String staffID, Color color, int event, Staff staff) {
    /*Se debe reemplazar el AuthenticationBloc por el 
  Bloc que se creara para los Eventos dentro del Home*/
    return Material(
      color: Colors.transparent,
      child: Container(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(24.0),
          highlightColor: Colors.blue[900],
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                fullscreenDialog: true,
                builder: (_) => StaffProfile(staff: staff),
              ),
            );
          },
          child: Material(
            color: Colors.blue[900],
            elevation: 12.0,
            shadowColor: Colors.black,
            borderRadius: BorderRadius.circular(24.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: _buildTabletRow(staffID, color, staff),
                  flex: 1,
                  fit: FlexFit.tight,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

//Junta y construye todo el contenido
  Widget buildContent(BuildContext context, bool useMobileLayout) {
    return Container(
      child: Center(
        child: staffFlag
            ? StaggeredGridView.countBuilder(
                crossAxisCount: 2,
                crossAxisSpacing: 20.0,
                mainAxisSpacing: 20.0,
                itemCount: staffList.length,
                padding:
                    const EdgeInsets.symmetric(vertical: 9.0, horizontal: 50.0),
                itemBuilder: (BuildContext context, int index) {
                  Staff actualStaff = staffList[index];
                  return useMobileLayout
                      ? myItems(actualStaff.id, Colors.blue[900], index,
                          context, actualStaff)
                      : myTabletItems(
                          actualStaff.id, Colors.blue[900], index, actualStaff);
                },
                staggeredTileBuilder: (int index) {
                  return StaggeredTile.extent(
                      1, MediaQuery.of(context).size.height * 0.22);
                },
              )
            : CircularProgressIndicator(),
      ),
    );
  }

  Widget buildContenT(BuildContext context, bool useMobileLayout) {
    return Container(
      child: Center(
          child: staffFlag
              ? ListView.builder(
                  itemCount: staffList.length,
                  itemBuilder: (BuildContext context, int index) {
                    Staff actualStaff = staffList[index];
                    return Row(
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: 50.0,
                              width: 50.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25.0),
                                  image: DecorationImage(
                                      image:
                                          NetworkImage(actualStaff.downloadUrl),
                                      fit: BoxFit.cover)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          actualStaff.employeeName,
                          style: TextStyle(fontSize: 14.0),
                        ),
                      ],
                    );
                  },
                )
              : CircularProgressIndicator()),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double shortestSide = MediaQuery.of(context).size.shortestSide;
    final bool useMobileLayout = shortestSide < 600;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appbar(context),
      body: SafeArea(
        child: Container(
          child: Stack(
            fit: StackFit.loose,
            children: <Widget>[
              Container(
                  height: MediaQuery.of(context).size.height * 0.14,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: buildTitleText(),
                  )),
              Container(
                margin: EdgeInsets.all(10.0),
                height: MediaQuery.of(context).size.height * 0.29,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'My Staff',
                      style: style16.copyWith(
                          color: Colors.blue[900], fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 150),
                  child: buildContent(context, useMobileLayout)),
            ],
          ),
        ),
      ),
    );
  }
}
