import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:home_services/pages/add_staff_page.dart';
import 'package:home_services/pages/client_page.dart';
import 'package:home_services/pages/staff_page.dart';
import 'package:home_services/services/firebase_auth_service.dart';
import 'package:home_services/utils/my_flutter_app_icons.dart';
import 'package:home_services/notifiers/add_member_notifier.dart';
import 'package:polygon_clipper/polygon_clipper.dart';
import 'package:provider/provider.dart';

TextStyle style = TextStyle(
    fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.blue[900]);

class HomePage extends StatelessWidget {
  Widget _appbar(BuildContext context) {
    return AppBar(
      title: Center(
        child: Container(
          padding: EdgeInsets.only(left: 75.0),
          child: AutoSizeText(
            'House Name',
            maxLines: 1,
            maxFontSize: 16.0,
            minFontSize: 13.0,
            style: style.copyWith(color: Colors.blue[900]),
          ),
        ),
      ),
      actions: <Widget>[
        Center(
          widthFactor: 1.5,
          child: InkWell(
            onTap: () {
              _signOut(context);
              print('SignOut presses');
            },
            child: Row(
              children: <Widget>[
                AutoSizeText(
                  'Logout',
                  maxLines: 1,
                  maxFontSize: 15.0,
                  minFontSize: 12.0,
                  style: style.copyWith(
                      fontWeight: FontWeight.bold, color: Colors.red),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    MyFlutterApp.exit,
                    color: Colors.red,
                    size: 22.5,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
      leading: Icon(Icons.menu, color: Colors.blue[900]),
      elevation: 0.0,
      bottomOpacity: 0,
      backgroundColor: Colors.white,
    );
  }

  Future<void> _signOut(BuildContext context) async {
    try {
      final auth = Provider.of<FirebaseAuthService>(context, listen: false);
      await auth.signOut();
    } catch (e) {
      print(e);
    }
  }

//Circular widget used in home
  Widget homeCircle(String name, Icon icon, Color color, double containerH,
      double containerW) {
    return Material(
      elevation: 6.0,
      shape:
          CircleBorder(side: BorderSide(color: Colors.blue[900], width: 2.0)),
      child: Container(
        height: containerH * 0.30, // height of the button
        width: containerW * 0.25,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 2,
              child: icon,
              fit: FlexFit.tight,
            ),
            Flexible(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 21.0),
                child: AutoSizeText(
                  name,
                  maxLines: 2,
                  maxFontSize: 16.0,
                  minFontSize: 13.0,
                  style: style.copyWith(color: color, fontSize: 16.0),
                  textAlign: TextAlign.center,
                ),
              ),
              fit: FlexFit.loose,
            ),
          ],
        )),
      ),
    );
  }

//The center widget used in home
  Widget homePolygon(String name, double containerH, double containerW) {
    return Center(
      child: Container(
        height: containerH * 0.40,
        width: containerH * 0.40,
        child: ClipPolygon(
          sides: 6,
          borderRadius: 5.0, // Default 0.0 degrees
          rotate: 90.0, // Default 0.0 degrees
          boxShadows: [
            PolygonBoxShadow(color: Colors.black, elevation: 1.0),
            PolygonBoxShadow(color: Colors.grey, elevation: 5.0)
          ],
          child: Container(
            height: 100,
            width: 100,
            color: Colors.blue[900],
            padding: EdgeInsets.all(10.0),
            child: Center(
              child: AutoSizeText(
                name,
                maxLines: 1,
                maxFontSize: 16.0,
                minFontSize: 13.0,
                style: style.copyWith(color: Colors.white, fontSize: 16),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget homeTabletPolygon(String name, double containerH, double containerW) {
    return Center(
      child: Container(
        height: containerH * 0.45,
        width: containerH * 0.45,
        child: ClipPolygon(
          sides: 6,
          borderRadius: 5.0, // Default 0.0 degrees
          rotate: 90.0, // Default 0.0 degrees
          boxShadows: [
            PolygonBoxShadow(color: Colors.black, elevation: 1.0),
            PolygonBoxShadow(color: Colors.grey, elevation: 5.0)
          ],
          child: Container(
            height: 100,
            width: 100,
            color: Colors.blue[900],
            child: Center(
              child: AutoSizeText(
                name,
                maxLines: 2,
                maxFontSize: 20.0,
                minFontSize: 13.0,
                style: style.copyWith(color: Colors.white, fontSize: 20),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget homeStack(BuildContext context, double containerH, double containerW,
      bool useMobileLayout) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment(0, 0),
          child: useMobileLayout
              ? homePolygon('Greenwood', containerH, containerW)
              : homeTabletPolygon('Greenwood', containerH, containerW),
        ),
        Align(
          alignment: Alignment(0.55, -0.75),
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (_) => StaffPage(),
                ),
              );
            },
            child: homeCircle(
                'My Staff',
                Icon(
                  Icons.verified_user,
                  color: Colors.blue[900],
                  size: 36,
                ),
                Colors.blue[900],
                containerH,
                containerW),
          ),
        ),
        Align(
          alignment: Alignment(-0.55, -0.75),
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (_) => ClientsPage(),
                ),
              );
            },
            child: homeCircle(
                'My Clients',
                Icon(
                  Icons.supervised_user_circle,
                  color: Colors.blue[900],
                  size: 36,
                ),
                Colors.blue[900],
                containerH,
                containerW),
          ),
        ),
        Align(
          alignment: Alignment(0.55, 0.75),
          child: InkWell(
            onTap: () {},
            child: homeCircle(
                'Add ISP',
                Icon(
                  Icons.add,
                  color: Colors.blue[900],
                  size: 36,
                ),
                Colors.blue[900],
                containerH,
                containerW),
          ),
        ),
        Align(
          alignment: Alignment(-0.55, 0.75),
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (_) => ChangeNotifierProvider<AddMemberNotifier>(
                      create: (context) => AddMemberNotifier(),
                      child: AddMemberPage()),
                ),
              );
            },
            child: homeCircle(
                'Add Client',
                Icon(
                  Icons.person_add,
                  color: Colors.blue[900],
                  size: 36,
                ),
                Colors.blue[900],
                containerH,
                containerW),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    double containerH = MediaQuery.of(context).size.height * 0.4,
        containerW = MediaQuery.of(context).size.width * 0.9;

    final double shortestSide = MediaQuery.of(context).size.shortestSide;
    final bool useMobileLayout = shortestSide < 600;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: _appbar(context),
      body: Center(
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 100.0,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Image.asset(
                  "assets/houseLogo.png",
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Text(
                'My Homes',
                style: style.copyWith(
                    color: Colors.blue[900], fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                    height: containerH,
                    width: containerW,
                    child: homeStack(
                        context, containerH, containerW, useMobileLayout)),
              ),
            )
          ],
        ),
      ),
    );
  }
}
