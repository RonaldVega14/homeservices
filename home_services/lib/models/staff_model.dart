import 'package:flutter/material.dart';

class Staff {
  final String id;
  final String employeeName;
  final String address;
  final String dateOfBirth;
  final String dateOfHire;
  final String downloadUrl;
  final String emergencyAddress;
  final String emergencyName;
  final String emergencyPhone;
  final String phone;
  final String position;
  final String relationship;
  final Education education;

  Staff(
      {@required this.id,
      @required this.address,
      @required this.dateOfBirth,
      @required this.dateOfHire,
      @required this.downloadUrl,
      @required this.emergencyAddress,
      @required this.emergencyName,
      @required this.emergencyPhone,
      @required this.employeeName,
      @required this.phone,
      @required this.position,
      @required this.relationship,
      @required this.education});
}

class Education {
  final String highSchoolName;
  final String highSchoolAddress;
  final String highSchoolFrom;
  final String highSchoolTo;
  final bool highSchoolGraduated;
  final String highSchoolDiploma;

  final String collegeName;
  final String collegeAddress;
  final String collegeFrom;
  final String collegeTo;
  final bool collegeGraduated;
  final String collegeDiploma;

  final String otherName;
  final String otherAddress;
  final String otherFrom;
  final String otherTo;
  final bool otherGraduated;
  final String otherDiploma;

  Education(
      {@required this.collegeAddress,
      @required this.collegeDiploma,
      @required this.collegeFrom,
      @required this.collegeGraduated,
      @required this.collegeName,
      @required this.collegeTo,
      @required this.highSchoolAddress,
      @required this.highSchoolDiploma,
      @required this.highSchoolFrom,
      @required this.highSchoolGraduated,
      @required this.highSchoolName,
      @required this.highSchoolTo,
      @required this.otherAddress,
      @required this.otherDiploma,
      @required this.otherFrom,
      @required this.otherGraduated,
      @required this.otherName,
      @required this.otherTo});
}

// Widget _statusSelection(var addMemberNotifier) {
//   return Consumer<AddMemberNotifier>(builder: (_, addMemberNotifier, __) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//       children: <Widget>[
//         Flexible(
//           child: Text(
//             'Status: ',
//             style: style16,
//           ),
//           flex: 3,
//           fit: FlexFit.tight,
//         ),
//         Flexible(
//             child: Radio(
//               activeColor: Colors.blue[900],
//               value: 0,
//               groupValue: addMemberNotifier.statusSelector(),
//               onChanged: (value) {
//                 addMemberNotifier.changeStatus(value);
//               },
//             ),
//             flex: 1,
//             fit: FlexFit.tight),
//         Flexible(
//           child: Text(
//             'Active',
//             style: style16,
//           ),
//           flex: 3,
//           fit: FlexFit.tight,
//         ),
//         Flexible(
//           child: Radio(
//             activeColor: Colors.blue[900],
//             value: 1,
//             groupValue: addMemberNotifier.statusSelector(),
//             onChanged: (value) {
//               addMemberNotifier.changeStatus(value);
//             },
//           ),
//           flex: 1,
//           fit: FlexFit.tight,
//         ),
//         Flexible(
//           child: Text(
//             'Inactive',
//             style: style16,
//           ),
//           flex: 3,
//           fit: FlexFit.tight,
//         ),
//       ],
//     );
//   });
// }
