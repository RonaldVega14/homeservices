import 'dart:io';

class StaffForm {
  String id;
  String name;
  bool status;
  File file;

  StaffForm({this.id, this.file, this.name, this.status});
}
