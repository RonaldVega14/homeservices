HomeServices is an app created for manage staff for a home group.

An app created with flutter for Tablet and Phone retrieving all the information from firebase.
State Management: Provider

The app features:

•	Splash screen with custom animations.

•	Login screen with firebase integration.

The app uses email and password for login.

•	Home Screen with custom layout.

•	Staff Page and Clients Page with Custom layout.

All the staff information is on a firebase database.
You can select the image from this page by tapping on the staff image circle.


•	Staff Profile Page. 

It’s a custom design using tab View for presenting all the information in a more user-friendly way.



•	Add new members page.

It’s a long form divided into different part along the screen to make better UX.

In this form you can add the staff picture by tapping the circle image icon and you will be redirect to the camera app.	

**Tablet Version**

Login Screen
<img src="screenshots/loginTablet.png" width = "600">

Home Screen
<img src="screenshots/homeTablet.png" width = "600">

Staff Page and Clients Page
<img src="screenshots/staffTablet.png" width = "600">

Staff Profile Page
<img src="screenshots/staffProfileTablet.png" width = "600">
<img src="screenshots/staffProfileTablet2.png" width = "600">

Add New Staff Page
<img src="screenshots/addStaffTablet.png" width = "600">

**Phone Version (not finished)**

Login Screen
<img src="screenshots/loginPhone.png" height = "350">

Home Screen
<img src="screenshots/homePhone.png" height = "350">

Staff Page
<img src="screenshots/staffPhone.png" height = "350">
